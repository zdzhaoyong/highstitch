# HighStitch: High Altitude Georeferenced Aerial Images Stitching for Rocking Telephoto Lens

## Introduction

A georeferenced ortho-photo built from aerial images is the basic resource for various of remote sensing applications.
As traditional low flight altitude aerial photographic survey is hard to handle large areas, higher altitude brings better survey efficiency, while a telephoto camera is required to maintain the resolution.
Since existed novel structure from motion (SfM) methods are not suitable to process low field-of-view images with high altitude, this letter presents a novel georeferenced ortho-photo stitching system for telephoto images which contain both translation and rotation movements.
A photo taken strategy is designed to guarantee the consistency quality according to the flight plan and camera intrinsic parameters.
The potential matching topology is obtained by projecting images to the prior ground plane and overlap scores are estimated for neighbors query, followed with a feature based keypoint matching.
The ground plane and camera poses are optimized by joint considering feature matches, GPS and gimbal rotation informations, while the final orthophoto is fused with a fast exposure leveling and weighted multi-band blending algorithm.
The experiments shows that our system is able to output novel quality orthophoto with high robustness, and we shared a trial version of our algorithm on website: \url{https://github.com/zdzhaoyong/highstitch}.

![Pipiline](./doc/pipeline.jpg)

If you use this code for your research, please cite our paper <a href="https://arxiv.org/abs/1902.07995">GSLAM: A General SLAM Framework and B
enchmark</a>:

```
@inproceedings{gslamICCV2019,
        title={GSLAM: A General SLAM Framework and Benchmark},
        author={Yong Zhao, Shibiao Xu, Shuhui Bu, Hongkai Jiang, Pengcheng Han},
        booktitle={Proceedings of the IEEE International Conference on Computer Vision},
        year={2019},
        organization={IEEE}
}
```

## Install

**Please make sure you are using Ubuntu 16.04, which is the only system tested for this project.**

This project uses [Svar](https://github.com/zdzhaoyong/Svar) for multiple language support, please compile and install it firstly.

```
pip3 install git+https://github.com/zdzhaoyong/Svar.git
```

Copy the svar module plugins to your path:

```
sudo cp ./svar_modules/* /usr/local/lib
```

Then please compile the c++ source code with cmake (optional, if you only use python, this is not required):

```
cd highstitch
mkdir build&&cd build
cmake ..&& sudo make install
```

## Tryout the stitching

Call with compiled C++ binary:

```
highstitch -task ./data/sample/task.json -out dom.png
```

Call with python interface:

```
python3 python/5_highstitch.py -task ./data/sample/task.json -out dom.png
```

This source code is only for trial with the following limits:

1. does not provide georeference information
2. tile resolution is limited to level 18
3. source image resolution is limited to 1920*1440
4. up to support 200 images

If you wanna use the code for commercial usage, please contact the author zd5945@126.com .

## The NPU HighStitch Dataset (Coming Soon)

More public image sequences of this project can be found at website: http://zhaoyong.win/npu-highstitch-dataset

## How to capture your own dataset

We provide some python scripts for mission planning, simulation and dataset capturing. The capturing is now only developed for DJI M300 drones with H20t payload and OSDK support.

Firstly, select a mission area in website:  http://geojson.io/ .

![geojson](./doc/geojson.png)

Copy the JSON content to file [area.json](./python/area.json). A mission simulation can be visualized and planned with script, the camera parameters should corresponding to the zoom level:

```
cd python
python3 ./1_mission_simulation -area area.json -height 300 -v_max 15 -kml mission.kml -cam '[1920,1440,3350.778698,3350.778698,960,720]'
```

![image-20210131145849081](./doc/simulation.png)

Run [2_rtmv_capture.py](./python/2_rtmv_capture.py) on Manifold 2C, the gimbal should rock like the following gif:

```
python3 python/2_rtmv_capture.py -height 300 -v_max 15 -cam '[1920,1440,3350.778698,3350.778698,960,720]'
```

![](./doc/gimbal.gif)

Import the mission to your remote and fly, a rtmv file will be captured in your folder, now you can visualize it in the mapwidget:

```
python3 python/3_rtmv_play.py -rtmv data/sample.rtmv -cam '[1920,1440,3350.778698,3350.778698,960,720]'
```

![image-20210131151422954](./doc/rtmv_play.png)

Generate a task from the captured rtmv file:

```
python3 python/4_task_generation.py -rtmv data/sample.rtmv -task data/sample -height 300 -v_max 15 -cam '[1920,1440,3350.778698,3350.778698,960,720]'
```

Stitch the task to orthophoto:

```
python3 python/5_highstitch.py -task ./data/sample/task.json -out dom.png
```

The ortho should looks like this:

![dom](./doc/dom.png)
