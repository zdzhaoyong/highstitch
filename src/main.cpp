#include "Svar/Registry.h"

using namespace sv;

auto highstitch=Registry::load("svar_highstitch");

int main(int argc,char** argv){
    svar.ParseMain(argc,argv);

    std::string lic=svar.arg<std::string>("license","","If you wanna activate, set this");
    std::string task=svar.arg<std::string>("task","","The task wanna process");
    std::string result=svar.arg<std::string>("out","dom.jpg","The result file to save");

    if(svar.get("help",false))
        return svar.help();

    if(lic.size()){
        return highstitch["activate"](lic).as<bool>();
    }

    if(task.empty()){
        std::cerr<<"Please set task :\n  highstitch -task <your_task_file>\n";
        return 0;
    }

    bool success=highstitch["stitch_task"](sv::Svar::loadFile(task),result).as<bool>();

    return success?0:-1;
}
